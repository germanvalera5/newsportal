asgiref==3.4.1
boto3==1.20.5
botocore==1.23.5
Django==3.2.9
django-environ==0.8.1
django-filter==21.1
django-phonenumber-field==5.2.0
django-storages==1.12.3
djangorestframework==3.12.4
djangorestframework-simplejwt==5.0.0
jmespath==0.10.0
phonenumberslite==8.12.36
PyJWT==2.3.0
python-dateutil==2.8.2
pytz==2021.3
s3transfer==0.5.0
six==1.16.0
sqlparse==0.4.2
psycopg2==2.8.0
urllib3==1.26.7
