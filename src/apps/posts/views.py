import django_filters.rest_framework
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from . import serializers
from .models import Post

User = get_user_model()


class PostViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.PostSerializer
    queryset = Post.objects.all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['title', 'text', 'topic']

    def get_serializer_class(self, *args, **kwargs):
        if self.action == 'partial_update':
            return serializers.PostSerializer
        if self.action == 'create':
            return serializers.PostCreateSerializer
        if self.action == 'multiple_update':
            kwargs.setdefault('context', self.get_serializer_context())
            return serializers.PostSerializer
        return serializers.PostListSerializer

    def get_queryset(self, *args, **kwargs):
        if self.request.user.user_type == User.CLIENT:
            return Post.objects.filter(user_id=self.request.user)
        if self.action == "corporate_posts":
            return Post.get_corporate_posts(self.request.user)
        return Post.objects.all()

    @action(
        detail=False,
        methods=['get'],
        url_path='corporate-posts',
    )
    def corporate_posts(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @action(methods=['patch'], detail=False)
    def multiple_update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instances = []
        for item in request.data:
            instance = get_object_or_404(self.get_queryset(), id=int(item['id']))
            serializer = super().get_serializer(instance, data=item, partial=partial)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            instances.append(serializer.data)
        return Response(instances)
