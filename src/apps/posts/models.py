from apps.companies.models import Company
from django.conf import settings
from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=100, verbose_name="Title")
    user_id = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="posts"
    )
    text = models.TextField(max_length=1000)
    topic = models.CharField(verbose_name="Topic", max_length=100)

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts: All"

    def __str__(self):
        return self.title

    @staticmethod
    def get_corporate_posts(user: object):
        if not user.company_id:
            return Post.objects.none()
        company = Company.objects.get(id=user.company_id.id)
        posts_qs = Post.objects.none()
        for employee in company.employees.all():
            posts_qs |= employee.posts.all()
        return posts_qs
