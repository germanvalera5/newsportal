from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import Post

User = get_user_model()


class UserDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
        )
        read_only_fields = ("id", )


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = "__all__"
        read_only_fields = (
            "user_id",
            "text",
            "topic",
        )


class PostCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            'id',
            'user_id',
            'title',
            'text',
            'topic',
        )
        read_only_fields = ('user_id', )

    def to_internal_value(self, data):
        dict_data = super(PostCreateSerializer, self).to_internal_value(data)
        dict_data['user_id'] = self.context['request'].user
        return dict_data


class PostListSerializer(serializers.ModelSerializer):
    user_id = UserDetailSerializer()

    class Meta:
        model = Post
        fields = (
            'id',
            'user_id',
            'title',
            'text',
            'topic',
        )
