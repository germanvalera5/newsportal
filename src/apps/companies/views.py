from apps.posts.models import Post
from apps.users.permissions import IsAdmin
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Company
from .serializers import CompanySerializer, CompanyDetailSerializer


class CompanyViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = [IsAdmin, ]

    def get_serializer_class(self, *args, **kwargs):
        if self.action == "details":
            return CompanyDetailSerializer
        return CompanySerializer

    def get_queryset(self, *args, **kwargs):
        if self.action == "corporate_posts":
            return Post.get_corporate_posts(self.request.user)
        return Company.objects.all()

    @action(
        detail=False,
        methods=['get'],
        url_path='details'
    )
    def details(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @action(
        methods=["get"],
        detail=False,
        url_path="info",
        permission_classes=[IsAuthenticated]
    )
    def get_info(self, request, *args, **kwargs):
        serializer = self.get_serializer(instance=request.user.company_id)
        return Response(data=serializer.data)
