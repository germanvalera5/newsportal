from django.db import models

from apps.users.validators import validate_file_size_10_mb


class Company(models.Model):
    name = models.CharField(verbose_name="Name", max_length=100)
    url = models.URLField(verbose_name="URL")
    address = models.CharField(max_length=150)
    date_created = models.DateField(verbose_name="")
    logo = models.FileField(
        upload_to="media/",
        validators=[
            validate_file_size_10_mb,
        ],
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = "Company"
        verbose_name_plural = "Companies: All"

    def __str__(self):
        return self.name
