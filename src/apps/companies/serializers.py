from apps.posts.models import Post
from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Company

User = get_user_model()


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'

    read_only_fields = ("id",)


class EmployeePostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            "id",
            "title",
            "text",
            "user_id",
        )


class EmployeeSerializer(serializers.ModelSerializer):
    posts = EmployeePostSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['username', 'email', "posts"]


class CompanyDetailSerializer(serializers.ModelSerializer):
    employees = EmployeeSerializer(many=True, read_only=True)

    class Meta:
        model = Company
        fields = (
            "id",
            "name",
            "url",
            "employees"
        )
        read_only_fields = ("id", )
