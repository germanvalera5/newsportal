from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from . import serializers
from .permissions import IsAdmin, IsSuperAdmin


User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.UserDetailSerializer
    permission_classes = [IsAdmin, ]

    def get_queryset(self, *args, **kwargs):
        if self.action == "make_admin":
            return User.objects.filter(user_type=User.CLIENT)
        if self.request.user.user_type == User.ADMIN:
            return User.objects.filter(user_type=User.CLIENT)
        return User.objects.all()

    def get_serializer_class(self, *args, **kwargs):
        if self.action == "register":
            return serializers.CreateUserSerializer
        if self.action == "partial_update" or \
                self.action == "update_my_profile":
            return serializers.UpdateUserSerializer
        return serializers.UserDetailSerializer

    @action(
        detail=False,
        methods=['post'],
        url_path='register',
        permission_classes=[AllowAny]
    )
    def register(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(
        detail=True,
        methods=['post'],
        url_path='make-admin',
        permission_classes=[IsSuperAdmin]
    )
    def make_admin(self, request, pk, *args, **kwargs):
        user = self.get_object()
        user.user_type = User.ADMIN
        user.save()
        return Response()

    @action(
        methods=["delete"],
        detail=True,
        url_path="soft-delete",
        permission_classes=[IsAdmin]
    )
    def soft_delete(self, request, pk, *args, **kwargs):
        user = self.get_object()
        user.delete_account()

        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(
        methods=["patch"],
        detail=False,
        url_path="update-my-profile",
        permission_classes=[IsAuthenticated]
    )
    def update_my_profile(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            instance=request.user, data=request.data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data)

    @action(
        methods=["get"],
        detail=False,
        url_path="info",
        permission_classes=[IsAuthenticated]
    )
    def get_info(self, request, *args, **kwargs):
        serializer = self.get_serializer(instance=request.user)
        return Response(data=serializer.data)
