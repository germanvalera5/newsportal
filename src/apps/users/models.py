import time

from django.db import models, transaction
from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager as BaseUserManager
from phonenumber_field.modelfields import PhoneNumberField

from .validators import validate_file_size_10_mb


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_superuser(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError("User must have an email")
        if not password:
            raise ValueError("User must have a password")
        user = self.model(
            email=self.normalize_email(email)
        )
        user.username = email
        user.set_password(password)
        user.is_staff = True
        user.is_superuser = True
        user.active = True
        user.user_type = User.SUPER_ADMIN
        user.save(using=self._db)

        return user


class User(AbstractUser):
    CLIENT = "Client"
    ADMIN = "Admin"
    SUPER_ADMIN = "Super admin"

    USER_TYPE_CHOICES = (
        (CLIENT, 'Client user'),
        (ADMIN, 'Admin user'),
        (SUPER_ADMIN, 'Super admin user')
    )
    email = models.EmailField("email address", unique=True)
    phone_number = PhoneNumberField(blank=True, default="")
    user_type = models.CharField(
        max_length=20, db_index=True,
        choices=USER_TYPE_CHOICES,
        default=CLIENT,
        null=True,
        blank=True,
    )
    company_id = models.ForeignKey(
        to="companies.Company",
        related_name="employees",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    avatar = models.FileField(
        upload_to="media/",
        validators=[
            validate_file_size_10_mb,
        ],
        null=True,
        blank=True
    )
    is_deleted = models.BooleanField(verbose_name="Is deleted?", default=False)

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users: All"

    def __str__(self):
        return self.get_full_name() or self.email

    def delete_account(self):

        dummy_text_default = "[Deleted]"

        with transaction.atomic():
            # 1. Mark user as deleted and inactive, update personal info
            # 1.1. Clear personal data
            self.email = "%s.%d.email@example.com" % (
                dummy_text_default,
                int(time.time()),
            )
            self.username = dummy_text_default + str(time.time())
            self.first_name = dummy_text_default
            self.last_name = dummy_text_default
            self.phone_number = ""
            self.is_active = False
            self.is_deleted = True
            self.user_type = None
            self.company_id = None

            self.save()
