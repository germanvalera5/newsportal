from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User
from .forms import UserCustomCreationForm, UserCustomChangeForm


@admin.register(User)
class UserAdmin(UserAdmin):
    add_form = UserCustomCreationForm
    form = UserCustomChangeForm

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'email')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        ('Avatar', {'fields': ('avatar', )}),
        ('Company', {'fields': ('company_id',)}),
        ('Phone number', {'fields': ('phone_number',)}),
    )

    list_display = (
        "id",
        "email",
        "user_type",
        "first_name",
        "last_name",
        "is_superuser",
        "is_staff",
        "company_id",
    )
    list_display_links = ("id", "email", )
