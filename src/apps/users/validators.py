from django.core.files.uploadedfile import TemporaryUploadedFile, InMemoryUploadedFile
from django.core.exceptions import ValidationError


def validate_file_size_10_mb(file_obj):
    """
    Custom validator to check uploaded file size is not exceeding 10MB.
    """
    if isinstance(file_obj, (TemporaryUploadedFile, InMemoryUploadedFile)):
        size = file_obj.size
    else:
        size = file_obj.file.size

    threshold_mb = 10
    threshold_bytes = threshold_mb * 1024 * 1024

    if size > threshold_bytes:
        raise ValidationError(
            ("You can only upload an image up to %dMB in size" % threshold_mb)
        )
