# Generated by Django 3.2.9 on 2021-11-13 14:17

import apps.users.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_user_avatar'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='avatar',
            field=models.FileField(blank=True, null=True, upload_to='media/', validators=[apps.users.validators.validate_file_size_10_mb]),
        ),
    ]
