from rest_framework import serializers
from django.contrib.auth import get_user_model
from rest_framework.validators import UniqueValidator

from apps.companies.serializers import CompanySerializer

from phonenumber_field import serializerfields
User = get_user_model()


class UserDetailSerializer(serializers.ModelSerializer):
    company_id = CompanySerializer(serializers.ModelSerializer)

    class Meta:
        model = User
        fields = '__all__'


class UpdateUserSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(
        required=False, max_length=50, help_text="First Name"
    )
    last_name = serializers.CharField(
        required=False, max_length=50, help_text="Last Name"
    )
    phone_number = serializerfields.PhoneNumberField(
        required=False,
        allow_blank=False,
        allow_null=False,
        max_length=30,
        help_text="Mobile Phone Number"
    )

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "phone_number",
            "avatar",
            "company_id",
            "username",
            "user_type",
        )


class CreateUserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(
        min_length=8, max_length=68, write_only=True
    )

    def create(self, validated_data):
        user = super(CreateUserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.is_active = True
        user.save()
        return user

    class Meta:
        model = User
        fields = '__all__'

