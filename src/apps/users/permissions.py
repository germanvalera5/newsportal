from rest_framework.permissions import BasePermission

from .models import User


class IsAdmin(BasePermission):
    """
    Allow access only to admins or super admins.
    """
    message = "admin account required"

    def has_permission(self, request, view):
        return request.user and request.user.user_type == User.ADMIN \
            or request.user.user_type == User.SUPER_ADMIN


class IsSuperAdmin(BasePermission):
    """
    Allow access only to super admins.
    """
    message = "super admin account required"

    def has_permission(self, request, view):
        return request.user and request.user.user_type == User.SUPER_ADMIN
