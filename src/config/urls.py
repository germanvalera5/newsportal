from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
urlpatterns = [
    path('admin/', admin.site.urls),
    # API urls
    path(
        'api/v1/',
        include(
            (
                [
                    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
                    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
                    path('', include(('apps.users.urls', "users"))),
                    path('', include(('apps.posts.urls', "posts"))),
                    path('', include(('apps.companies.urls', "companies"))),
                ],
                "api",
            ),
            namespace="api",
        ),
    ),
]
