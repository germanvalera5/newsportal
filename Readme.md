- create virtual environment `python3 -m venv venv`
- build project from docker-compose `make build`
- migrate db: `make migrate`
- create superadmin `make superadmin`
- run server: `make server`

Install Postman documentation via importing postman_collection.json file in Postman app

example of credentials of created users for retrieving a token:
- email: user1@example.com
- password: useruser
